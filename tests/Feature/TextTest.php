<?php

namespace Tests\Feature;

use App\Text;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class TextTest extends TestCase
{

    public function testValidate(){

        $emptyText = "";
        $bigText = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam imperdiet tellus a pellentesque elementum. Nulla id nulla quis nulla egestas imperdiet nec et quam. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Ut sed euismod diam. In nec neque mattis, egestas lacus quis, tristique ipsum. Nulla consequat ligula sit amet est tincidunt imperdiet. Duis consequat augue nec ex elementum placerat. Aenean accumsan eros nisl, ac facilisis justo pretium id. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.";
        $text = new Text();
        $this->assertEquals('The text can not be empty!',$text->validate($emptyText));
        $this->assertEquals('The text can not be longer than 255 chars!',$text->validate($bigText));

    }

    public function testPrepareColumns(){
        $text = new Text();
        $results = $text->prepareColumns();
        //$this->assertCount('1', $results); Fail line
        $this->assertCount('5', $results);


    }
}
