<?php

namespace Tests\Feature;

use App\Game;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class GameTest extends TestCase
{
    /**
     * @dataProvider valuesProvider
     */

    public function testCalcChance($valueA, $valueB, $total){

        $game = new Game();
        $chances = $game->calcChance($valueA,$valueB);

        $this->assertEquals($total, $chances);

    }

    public function valuesProvider(){
        return [
            [1,10,10.00],
            [1,52,1.92],
            [1,23,4.35],
        ];
    }


}
