<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Text extends Model
{
    public function analyzeText($text){
         $text = strtolower($text);
         $text = $this->prepareArray($text);
         $result = $this->group_by('index', $text);
         return $result;

    }

    public function prepareArray($text){

        $elements = array();
        $text = str_replace(' ','',$text);
        $strings = str_split($text);
        $occurrences = array_count_values($strings);
        $arraySize = count($strings);
        $before = "";

        for($i=0;$i<$arraySize;$i++){
            $position1 = stripos($text, $strings[$i]);
            $position2 = strrpos($text, $strings[$i]);
            $longest= $position2 - $position1;
            $key = array_search($strings[$i], $strings);
            $after = $key;
            $elements[] =  array(
                'index' => $strings[$i],
                'occurrences' => $occurrences[$strings[$i]],
                'after'=> $before,
                'before' => next($strings),
                'longest' => $longest
            );
            $before = $strings[$i];
            unset($strings[$key]);
        }

        return $elements;
    }



    public function prepareColumns(){

        $columns = [
            'string' => [
                'name' => 'Strings'
            ],

            'occurrences' => [
            'name' => 'Occurrences'
            ],

            'before' => [
            'name' => 'Before'
            ],

            'after' => [
                'name' => 'After'
            ],

            'longest' => [
                'name' => 'Longest'
            ]
        ];


        return $columns;
    }

    public function validate($text){
        $message = null;
        if (strlen($text)>255){
            $message = "The text can not be longer than 255 chars!";
        }
        if(strlen($text)<1){
            $message = "The text can not be empty!";
        }

        return $message;
    }

    private function group_by($key, $data) {

        $result = array();
        foreach($data as $val) {
            if(array_key_exists($key, $val)){
                $result[$val[$key]][] = $val;
            }else{
                $result[""][] = $val;
            }
        }

        return $result;
    }
}