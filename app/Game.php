<?php

namespace App;

use Illuminate\Database\Eloquent\Model;



class Game extends Model
{

    public function chooseCard($currentDeck, $totalDeck){
        $deck = new Deck($totalDeck);
        $card = $deck->drawCard($currentDeck);
        return $card;
    }

    public function removeCard($currentDeck, $card){
        $key = array_search($card, $currentDeck);
        unset($currentDeck[$key]);
        $leftDeck = array_values($currentDeck);
        return $leftDeck;
    }

    public function calcChance($events,$outcomes){
        $chances = $events/$outcomes * 100;
        return number_format($chances, 2);
    }

    public function hasMatched($hand, $card, $currentDeck){
        if($hand == $card){
            $s = $this->calcChance(1, $currentDeck);
            $message = "Got it. The chance was {$s} %";
            return $message;
        }

    }

}
