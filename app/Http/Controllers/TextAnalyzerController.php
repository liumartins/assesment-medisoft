<?php


namespace App\Http\Controllers;

use App\Text;
use Illuminate\Http\Request;
class TextAnalyzerController extends Controller
{

    public function index(){
        return view('textanalyzer/index');
    }

    public function analyze(Request $request){

        $analyze = new Text();
        $text = $request->get('text');
        $message = $analyze->validate($text);
        $data = $analyze->analyzeText($text);
        $columns = $analyze->prepareColumns();

        return view('textanalyzer/analyze')->with(compact('data', 'columns', 'message'));



    }
}