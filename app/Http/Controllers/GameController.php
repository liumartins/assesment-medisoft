<?php


namespace App\Http\Controllers;

use App\Deck;
use App\Game;
use Illuminate\Http\Request;

class GameController extends Controller
{
    public function index(){

        return view('game/index', compact('name'));
    }


    public function start(Request $request){

        $deck = new Deck(52);
        $deck = $deck->shuffleDeck();
        $totalDeck = count($deck);
        $request->session()->put('deck', $deck);
        $request->session()->put('totalDeck', $totalDeck);
        return view('game/start', compact('deck'));
    }

    public function chooseCard(Request $request){
        $deck = $request->session()->get('deck');
        $totalDeck = $request->session()->get('totalDeck');
        $hand = new Game();
        $hand = $hand->chooseCard($deck,$totalDeck);
        $request->session()->put('hand', $hand);
        return view('game/choosecard', compact('deck','hand'));
    }

    public function play(Request $request){

        $deck = $request->session()->get('deck');
        $hand = $request->session()->get('hand');
        $totalDeck = $request->session()->get('totalDeck');
        $game = new Game();
        $card = $game->chooseCard($deck, $totalDeck);
        $hasMatched = $game->hasMatched($hand, $card,$totalDeck);

        if($hasMatched != null){

            $message =  $request->session()->put('message', $hasMatched);
            $card = $request->session()->put('card', $card);
            return redirect()->action('GameController@success');

        } else {

            $deck = $game->removeCard($deck, $card);
            $totalDeck = count($deck);
            $request->session()->forget('deck');
            $request->session()->forget('totalDeck');
            $request->session()->put('totalDeck', $totalDeck);
            $request->session()->put('card', $card);
            $request->session()->put('deck', $deck  );


        }

        return view('game/play', compact('deck', 'hand', 'card', 'totalDeck'));
    }

    public function success(Request $request){
        $deck = $request->session()->get('deck');
        $hand = $request->session()->get('hand');
        $card = $request->session()->get('card');
        $message = $request->session()->get('message');

        return view('game/success', compact('deck', 'hand', 'card', 'message'));

        $request->session()->flush();

    }

}