<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Deck extends Model
{

    private $_deckTotalCards;


    public function __construct($deckTotalCards){
        $this->_deckTotalCards = $deckTotalCards;
    }

    public function drawCard($deck){
        return $deck[mt_rand(0, count($deck) - 1)];
    }

    public function shuffleDeck(){
        $deck = $this->createDeck();
        $i = count($deck);
        while(--$i) {
            $j = mt_rand(0, $i);

            if ($i != $j) {
                $tmp = $deck[$j];
                $deck[$j] = $deck[$i];
                $deck[$i] = $tmp;
            }
        }
        return $deck;
    }


    private function createDeck(){
        $deck = array();
        $suits = $this->prepareSuits();
        $values = $this->prepareValues();
        foreach ($suits as $suit){
            foreach ($values as $value){
                $deck[] = $suit.$value;
            }
        }
        return $deck;
    }

    private function prepareSuits(){
        $suits = [ "H", "D", "C", "S" ];
        return $suits;
    }

    private function prepareValues(){
        $values = [2,3,4,5,6,7,8,9,10,'J','Q','K','A'];
        return $values;
    }
}
