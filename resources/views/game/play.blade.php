
<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

    <!-- Styles -->
    <style>
        html, body {
            background-color: #fff;
            color: #636b6f;
            font-family: 'Nunito', sans-serif;
            font-weight: 200;
            height: 100vh;
            margin: 0;
        }

        .full-height {
            height: 100vh;
        }

        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
        }

        .position-ref {
            position: relative;
        }


        .content {
            text-align: center;
        }

        .title {
            font-size: 26px;
        }

        .links > a {
            color: #636b6f;
            padding: 0 25px;
            font-size: 13px;
            font-weight: 600;
            letter-spacing: .1rem;
            text-decoration: none;
            text-transform: uppercase;
        }
        .m-b-md {

        }
    </style>
</head>
<body>

<div class="flex-center position-ref full-height">

    <div class="content">
        <div>

            <div>
                <h3>The Current the deck:
                    <br/>
                    <?php

                        foreach ($deck as $dk){
                            echo $dk . ' - ';
                        }
                    ?>
                </h3>
            </div>
            <br/>
            <hr/>
            <h2>That's is the Dealer hand: <?php echo $card; ?></h2>
            <hr/>
            <br/>
            <h2>That's is your hand: <?php echo $hand; ?></h2>
            <hr/>
            <br/>
            <button><a href="{{ url('game/play') }}">Draft a Card</a> </button>
        </div>
    </div>
</div>
</body>
</html>




