<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

    <!-- Styles -->
    <style>
        html, body {
            background-color: #fff;
            color: #636b6f;
            font-family: 'Nunito', sans-serif;
            font-weight: 200;
            height: 100vh;
            margin: 0;
        }

        .title {
            font-size: 84px;
        }

        .links > a {
            color: #636b6f;
            padding: 0 25px;
            font-size: 13px;
            font-weight: 600;
            letter-spacing: .1rem;
            text-decoration: none;
            text-transform: uppercase;
        }

        .m-b-md {
            margin-bottom: 30px;
        }
    </style>
</head>
<body>
<div class="flex-center position-ref full-height">
    @if (Route::has('login'))
        <div class="top-right links">
            @auth
                <a href="{{ url('/home') }}">Home</a>
            @else
                <a href="{{ route('login') }}">Login</a>

                @if (Route::has('register'))
                    <a href="{{ route('register') }}">Register</a>
                @endif
            @endauth
        </div>
    @endif

    <div class="content">
        <div><h2>Analyze Table</h2></div>

            <?php if($message):?>
                <div>
                    <em><?php echo $message;?></em>
                    <a href="{{ url('/textanalyzer') }}">Get Back</a>
                </div>
            <?php else: ?>
            <table>
                <thead>
                <tr>
                <?php foreach($columns as $column): ?>
                    <th><?php echo $column['name']?></th>
                <?php endforeach;?>
                </tr>
                </thead>
                <tbody>

                    <?php foreach($data as $dt):?>
                    <tr>
                        <td><?php echo($dt['0']['index']) ?></td>
                        <td><?php echo($dt['0']['occurrences']) ?></td>

                            <td>
                            <?php if(count($dt)>1):?>
                                <?php foreach ($dt as $val){
                                    echo($val['before']);
                                }?>
                            <?php else: ?>
                                <?php echo(($dt['0']['before'] == ''? 'none': $dt['0']['before'] )); ?>
                            <?php endif; ?>
                            </td>
                            <td>
                            <?php if(count($dt)>1):?>
                                <?php foreach ($dt as $val){
                                    echo($val['after']);
                                }?>
                            <?php else: ?>
                                <?php echo(($dt['0']['after'] == ''? 'none': $dt['0']['after'] )); ?>
                            <?php endif; ?>
                            </td>
                        <td><?php echo($dt['0']['longest']) ?></td>
                    </tr>
                        <?php endforeach;?>

                </tbody>

            </table>
        <?php endif;?>
        </div>

    </div>
</div>
</body>
</html>
<pre>

