<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/game', 'GameController@index');
Route::get('/game/start', 'GameController@start');
Route::get('/game/play', 'GameController@play');
Route::get('/game/choosecard', 'GameController@chooseCard');
Route::get('/game/success', 'GameController@success');
Route::get('/textanalyzer', 'TextAnalyzerController@index');
Route::get('/textanalyzer/analyze', 'TextAnalyzerController@analyze');